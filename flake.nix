{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    impermanence.url = github:nix-community/impermanence;
    # Uncomment to include sops-nix in inputs. Also uncomment in modules below
    # sops-nix.url = github:Mic92/sops-nix;
  };
  outputs = { self, nixpkgs, sops-nix, ... }@inputs:
    let
      system = "x86_64-linux";
    in
      {
        nixosConfigurations = {
          demo = nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              ./configuration.nix
              # sops-nix.nixosModules.sops
            ];
            specialArgs = { inherit inputs; };
          };
        };
      };
}
